import React, { Component } from 'react';
import Counter from '../components/Counter/Index';

class CounterPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            count: 0
        };
    }

    handleOnClick = () => {
        console.log("click!");
        this.setState({ count: this.state.count + 1 });
    }

    render() {
        return (
            <div className='text-center'>
                <Counter
                    count={this.state.count}
                    handleOnClick={this.handleOnClick}
                />
            </div>
        );
    }
}

export default CounterPage;