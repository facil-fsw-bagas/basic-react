import React from "react";
import Article from "../components/Article/Index";

function Articles() {

    let persons = [
        {
            id: 1,
            img: "https://i.imgur.com/1bX5QH6.jpg",
            title: "Lin Lanying",
            role: "Scientist",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur interdum tincidunt velit, vitae sagittis dui lobortis et. Maecenas pharetra ultrices accumsan. Nam risus arcu, imperdiet quis aliquam vitae, eleifend venenatis risus. Vestibulum in viverra quam. Cras lacinia elit at efficitur hendrerit. Vivamus vel odio a sapien imperdiet elementum. Suspendisse ut velit maximus, efficitur tortor non, iaculis est. Nullam fringilla turpis in elit semper, ac dignissim turpis gravida. Maecenas tempor arcu eget bibendum rhoncus. Praesent efficitur dui eleifend orci accumsan, id imperdiet dolor laoreet. Integer auctor, augue id tempor lobortis, ipsum ante ultrices ipsum, a aliquam leo tellus ut nibh. Vestibulum nec diam vel magna mollis vehicula."
        },
        {
            id: 2,
            img: "https://i.imgur.com/OKS67lh.jpg",
            title: "Aklilu Lemm",
            role: "Researcher",
            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur interdum tincidunt velit, vitae sagittis dui lobortis et. Maecenas pharetra ultrices accumsan. Nam risus arcu, imperdiet quis aliquam vitae, eleifend venenatis risus. Vestibulum in viverra quam. Cras lacinia elit at efficitur hendrerit. Vivamus vel odio a sapien imperdiet elementum. Suspendisse ut velit maximus, efficitur tortor non, iaculis est. Nullam fringilla turpis in elit semper, ac dignissim turpis gravida. Maecenas tempor arcu eget bibendum rhoncus. Praesent efficitur dui eleifend orci accumsan, id imperdiet dolor laoreet. Integer auctor, augue id tempor lobortis, ipsum ante ultrices ipsum, a aliquam leo tellus ut nibh. Vestibulum nec diam vel magna mollis vehicula."
        }
    ];

    return (
        <div>
            {persons.map(person => {
                return (
                    <Article key={person.id} person={person} />
                );
            })}
        </div>
    );
}

export default Articles;
