import React from "react";

function Input({ name, value, handleOnChange, placeholder }) {
    return (
        <input
            className="form-control" 
            name={name} 
            value={value} 
            onChange={handleOnChange}
            placeholder={placeholder}
        />
    );
}

export default Input;