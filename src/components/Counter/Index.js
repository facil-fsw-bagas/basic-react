import React, { Component } from 'react';

class Counter extends Component {
    // Intialization
    constructor(props) {
        super(props);
        this.state = {};
        console.log("component: initialization");
    }

    // Mount, Update, Unmount
    static getDerivedStateFromProps(props, state) {
        console.log("component: get derived state from props");
        return {};
    }

    // Mount
    componentDidMount() {
        console.log("component: did mount");
    }

    shouldComponentUpdate() {
        console.log("component: should update");
        return true;
    }

    // Update
    componentDidUpdate() {
        console.log("component: did update");
    }

    // Unmount
    componentWillUnmount() {
        console.log("component: will unmount");
    }

    render() {
        console.log("component: mount");
        const { count, handleOnClick, handleOnDoubleClick } = this.props;
        return (
            <div onClick={handleOnClick} onDoubleClick={handleOnDoubleClick}>
                clicked
                <h1 className="label label-default">{count}</h1> times
            </div>
        );
    }
}

export default Counter;